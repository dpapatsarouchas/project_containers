FROM docker.io/bitnami/spark:2.4.5-debian-10-r49

USER root
COPY spark.requirements.txt /usr/local/temp/requirements.txt
RUN pip3 install -r /usr/local/temp/requirements.txt
USER 1001