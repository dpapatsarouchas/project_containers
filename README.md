# Course python Project
## Description of the project
The main purpose of this project is to get familiarize with container technology through the development of a simple application using Docker  
I choose to develope an app to test Latent Semantic Analysis techniques on articles from Wikipedia, using [Apache Spark](https://spark.apache.org/)

## How to run
To run the application you need to install [Docker](https://docs.docker.com/get-docker/) and [Docker Compose](https://docs.docker.com/compose/install/).  
With docker isntalled open a terminal and follow those steps:
1. git clone 
2. cd <project_folder>
3. docker-compose up --build -d

Next open a browser window and type [localhost:5000](http://localhost:5000) for the main application.
You can access [localhost:8080](http://localhost:8080) for information about Apache Spark running instances

(Apache spark is configured to use 2 workers with 2 cores and 2gb of ram each)