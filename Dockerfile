FROM ubuntu:18.04

ENV CELERY_BROKER_URL redis://redis:6379/0
ENV CELERY_RESULT_BACKEND redis://redis:6379/0
ENV C_FORCE_ROOT true

RUN mkdir -p /app

RUN apt-get update -y && \
    apt-get install -y python3 python3-pip openjdk-8-jdk ant && \
    update-java-alternatives -s java-1.8.0-openjdk-amd64 --jre-headless && \
    ln -s /usr/bin/python3 /usr/bin/python && \
    rm -rf /var/lib/apt/lists/* \
    rm -rf /var/cache/oracle-jdk8-installer

WORKDIR /app
COPY requirements.txt /app
RUN pip3 install -r requirements.txt --no-cache-dir