#!/usr/bin/python3
import requests
import os

def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]

def get_titles(title):
    S = requests.Session()

    URL = "https://en.wikipedia.org/w/api.php"

    PARAMS = {
        "action": "query",
        # "cmtitle": "Category:Concepts in physics",
        "cmtitle": title,
        "cmlimit": "10",
        "list": "categorymembers",
        "format": "json"
    }

    R = S.get(url=URL, params=PARAMS)
    DATA = R.json()

    PAGES = DATA['query']['categorymembers']

    return list(map(lambda x: x['title'], DATA['query']['categorymembers']))


def get_articles(category, limit = 500):
    if 'Category' not in category:
        category = f"Category:{category}"
    titles = get_titles(category)
    articles = list(filter(lambda x: 'Category' not in x, titles))
    categories = list(filter(lambda x: 'Category' in x, titles))
    result = articles

    if len(result) > limit:
        return result[:limit]
    
    if len(categories):
        for cat in categories:
            if len(result) >= limit:
                break
            result.extend(get_articles(cat, limit-len(result)))
    
    return result


def get_xml(titles: list):
    titles = '%0A'.join(titles)
    URL = f'https://en.wikipedia.org/w/index.php?title=Special:Export&pages={titles}'
    S = requests.Session()
    R = S.get(url=URL)
    return R.content


def download_articles(categories, opath, category_limit = 500):
    """
    ### Download articles for the given categories
    Example:\n
    `categories = ['Physics', 'Countries']`\n
    `download_articles(categories, '/usr/data/wikipedia/', category_limit=200)`
    """
    articles = [item for category in categories for item in get_articles(category, category_limit) ]
    os.makedirs(opath, exist_ok=True)
    # limit 300 articles per xml
    # if category_limit*categories > 300 create diferent xml files
    if len(categories) * category_limit > 300:
        article_chunks = list(chunks(articles, 300))
        for i,article_chunk in enumerate(article_chunks):
            target_path = os.path.join(opath, f'dump-{i}.xml')
            with open(target_path, 'wb') as f:
                f.write(get_xml(article_chunk))
    else:
        target_path = os.path.join(opath, 'dump.xml')
        with open(target_path, 'wb') as f:
            f.write(get_xml(articles))
    print('Done')

if __name__ == "__main__":
    # Get articles of category
    # category_title = "Physics"
    # article_titles = get_articles(category_title, 20)
    # print(article_titles)

    # Export articles into file
    # get_xml(['Physics', 'Active and passive transformation'])

    # Download articles for the given categories
    categories = ['Physics', 'Countries']
    download_articles(categories, '/usr/data/wikipedia/', category_limit=200)