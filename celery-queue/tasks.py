import os
import time
from celery import Celery

from wikipedia_tasks import download_articles


CELERY_BROKER_URL = os.environ.get('CELERY_BROKER_URL', 'redis://localhost:6379'),
CELERY_RESULT_BACKEND = os.environ.get('CELERY_RESULT_BACKEND', 'redis://localhost:6379')

celery = Celery('tasks', broker=CELERY_BROKER_URL, backend=CELERY_RESULT_BACKEND)


@celery.task(name='tasks.add')
def add(x: int, y: int) -> int:
    time.sleep(5)
    return x + y

@celery.task(name='tasks.some_long_task')
def some_long_task(d,c):
    time.sleep(5)
    with open('/usr/data/async_task.txt', 'w') as f:
        f.write(d)
    return c()
    # return "Done"


@celery.task(name='tasks.dl_articles')
def dl_articles(terms: str, limit=500) -> str:
    categories = terms.split(',')
    if not os.path.exists('/usr/data/wikipedia'):
        os.mkdir('/usr/data/wikipedia')
    for file in os.listdir('/usr/data/wikipedia'):
        os.remove(os.path.join('/usr/data/wikipedia',file))
    download_articles(categories, '/usr/data/wikipedia/', category_limit=limit)
    return "Done"