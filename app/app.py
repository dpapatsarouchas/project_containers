from flask import Flask, request
from flask import render_template
from flask.helpers import url_for
import wget
from spark_jobs import pipeline
from worker import celery
import celery.states as states
import os

app = Flask(__name__)

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/check/<string:task_id>')
def check_task(task_id: str) -> str:
    categories = request.args.get('categories')
    res = celery.AsyncResult(task_id)
    if res.state == states.PENDING:
        status = res.state
    else:
        status = str(res.result)
    return render_template('/categories/task_state.html', status=status, categories=categories)

# Forms
@app.route('/submit_categories', methods=['POST'])
def submit_categories():
    message = {}
    link = None
    try:
        categories = request.form['categories']
        limit = int(request.form['limit']) if request.form['limit'] else 500
        message['title'] = "Success"
        message['body'] = f"Downloading wikipedia content for the given categories {categories}"
        task = celery.send_task('tasks.dl_articles', kwargs={'terms':categories, 'limit': limit})
        link = url_for('check_task', task_id=task.id, categories=categories, external=True)
    except Exception as e:
        message['title'] = "Error"
        message['body'] = "Please try again"
        message['error'] = str(e)
    
    return render_template('categories/submition.html', message=message, link=link)

@app.route('/select_categories')
def select_categories():
    return render_template('categories/form.html', page_title='Select wikipedia categories')


@app.route('/top_terms_in_concepts')
def top_terms_in_concepts():
    # First check if wikipedia dump exists
    if not os.path.exists('/usr/data/wikipedia'):
        os.mkdir('/usr/data/wikipedia')
    if not len(os.listdir('/usr/data/wikipedia')):
        error = 'You have to run <b>"Select Categories"</b> first to download wikipedia dumps'
        return render_template('spark_results/top_terms_in_concepts.html', page_title='Top terms in concepts', error=error)
    n = 10
    terms = pipeline(f='top_terms_in_concepts', args={"n": n})
    return render_template('spark_results/top_terms_in_concepts.html', page_title='Top terms in concepts', items=terms)