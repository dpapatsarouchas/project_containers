import re
import gensim
import nltk
nltk.download('stopwords',download_dir='/usr/data', quiet=True)
nltk.data.path.append('/usr/data')
from nltk.corpus import stopwords

special_chars = re.compile("[@$/#.ô€€€:&*+=\\[\\]?!()){},\\'\">_<;%\\s]+")
stop_words = stopwords.words("english")

function_words = "a about above after again against ago ahead all almost along already also although always am among an and any are aren't around as at away backward backwards be because before behind below beneath beside between both but by can cannot can't cause 'cos could couldn't despite did didn't do does doesn't don't down during each either even ever every except for forward from had hadn't has hasn't have haven't he her here hers herself him himself his how however if in inside inspite instead into is isn't it its itself just 'll least less like 'm many may mayn't me might mightn't mine more most much must mustn't my myself near need needn't needs neither never no none nor not now of off often on once only onto or ought oughtn't our ours ourselves out outside over past perhaps quite 're rather 's seldom several shall shan't she should shouldn't since so some sometimes soon than that the their theirs them themselves then there therefore these they this those though through thus till to together too towards under unless until up upon us used usedn't usen't usually 've very was wasn't we well were weren't what when where whether which while who whom whose why will with without won't would wouldn't yet you your yours yourself yourselves"

function_words = function_words.split()

junk_words = 'disambiguation article refer ref related onlyinclude'
junk_words = junk_words.split()


def remove_function_words(text):
    return ' '.join([word for word in text.split() if word not in function_words])

def remove_junk_words(text):
    return ' '.join([word for word in text.split() if word not in junk_words])

def pre_process(text):
    
    text = text.lower()
    text = gensim.parsing.preprocessing.strip_tags(text) 
    text = gensim.parsing.preprocessing.strip_punctuation(text)
    text = gensim.parsing.preprocessing.strip_numeric(text) 
  
    text = remove_function_words(text)
    text = remove_junk_words(text)
    
    text = gensim.parsing.preprocessing.remove_stopwords(text) 
    
    stemmer = gensim.parsing.porter.PorterStemmer()
    text = stemmer.stem(text)
    text = gensim.parsing.preprocessing.strip_short(text, minsize=3) 
  
    
    return text

def count(text):
    unique_words = set(text)
    words_list = list(unique_words)
    res_dict = {}
    for word in unique_words:
        res_dict[word] = text.count(word)
    return res_dict