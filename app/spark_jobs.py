import math
import re
import numpy as np
import pandas as pd
import os
import time

from operator import add
from pyspark.mllib.linalg import SparseVector
from pyspark.mllib.common import JavaModelWrapper
from pyspark.mllib.linalg.distributed import IndexedRow, IndexedRowMatrix, RowMatrix
from sklearn.preprocessing import normalize
from pyspark.mllib.stat import Statistics
from pyspark import SparkContext
from pyspark.conf import SparkConf
from pyspark.sql import SparkSession
import sparknlp
from sparknlp.pretrained import PretrainedPipeline, Lemmatizer
import wikipedia_processes

class Sspark:
    def __init__(self, configs=None) -> None:
        self.conf = SparkConf().setAll([
            ('spark.executor.memory', '2g'),
            ('spark.executor.cores', '2')
        ])
        if configs:
            self.conf = SparkConf().setAll(configs)
        self.session = (SparkSession.\
                    builder.\
                    appName("spark_jobs").\
                    master("spark://spark:7077").\
                    config(conf=self.conf).\
                    getOrCreate())
    
    def __enter__(self):
        return self.session
    
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.session.stop()

def get_spark(configs=None):
    base_conf = [
        ('spark.executor.memory', '2g'),
        ('spark.executor.cores', '2')
    ]
    conf = SparkConf().setAll(base_conf)
    if configs:
        conf = SparkConf().setAll(base_conf+configs)
    spark = (SparkSession.\
        builder.\
        appName("spark_jobs").\
        master("spark://spark:7077").\
        config(conf=conf).\
        getOrCreate())
    print(spark.version)
    return spark

def summary_stats(spark, df: pd.DataFrame):
    results = {}
    for col in df.columns:
        print(f'column name ------ {col}')
        summary = Statistics.colStats(df[col])
        results[col] = {
            'mean': summary.mean(),
            'variance': summary.variance(),
            'numNonzeros': summary.numNonzeros
        }
    return results


def nlp():
    #create or get Spark Session
    # spark = sparknlp.start()
    spark = get_spark([("spark.jars.packages", "com.johnsnowlabs.nlp:spark-nlp_2.11:2.7.1")])
    #download, load, and annotate a text by pre-trained pipeline
    pipeline = PretrainedPipeline('recognize_entities_dl', 'en', disk_location='/usr/data/libs/recognize_entities_dl')
    result = pipeline.annotate('The Mona Lisa is a 16th century oil painting created by Leonardo')
    print(result)

def clear_xml(path, target_path, tags):
    # spark = get_spark([('spark.jars.packages','HyukjinKwon:spark-xml:0.1.1-s_2.10'),('spark.jars.packages','com.databricks:spark-xml_2.12:0.11.0')])
    spark = get_spark([('spark.jars.packages','com.databricks:spark-xml_2.11:0.11.0')])
    sc: SparkContext = spark.sparkContext
    # Do something
    df = spark.read.format('com.databricks.spark.xml').options(rowTag='book', rootTag='catalog').load('/usr/data/dummy.xml')
    print(df.printSchema())
    print(df.count())
    print(df.select('author').show())
    # TODO make this work
    # df.select("author", "title").write \
    #     .mode('append') \
    #     .format('xml') \
    #     .options(rowTag='book', rootTag='books') \
    #     .save('/usr/data/test')
    spark.stop()


def pipeline(f=None, args={}):
    spark = get_spark([('spark.jars.packages','com.databricks:spark-xml_2.11:0.11.0')])
    sc: SparkContext = spark.sparkContext
    sc.addPyFile('wikipedia_processes.py')
    xmls = list(map(lambda x: '/usr/data/wikipedia/'+x, os.listdir('/usr/data/wikipedia')))
    if len(xmls) == 0:
        return "Please select categories to download articles first"
    xmls_join = ','.join(xmls)
    df = spark.read.format('com.databricks.spark.xml').options(rowTag='page', rootTag='mediawiki').load(path=xmls_join).rdd
    # doc_ids = df.map(lambda x: x['id'])
    # print(df.take(2)[0]['id'])
    # print(df.take(2)[1]['id'])
    # print(doc_ids.collect())
    doc_titles = df.map(lambda x: x['title'])
    # print(df.take(1)[0]['revision']['text'][0])
    df = df.map(lambda x: x['revision']['text'][0]).filter(lambda x: len(x) > 0).map(wikipedia_processes.pre_process)

    # Number of docs
    num_docs = df.count()
    # print(num_docs)

    #term freq
    tf = df.map(lambda x: wikipedia_processes.count(x.split()))

    #document term freq
    doc_term_freq = tf.flatMap(lambda x: x.keys()).map(lambda word: (word, 1)).reduceByKey(add)
    # print(doc_term_freq.count())

    #TAKE TOP 50000 TERMS
    N = 50000
    doc_term_freq = sc.parallelize(doc_term_freq.top(N, key=lambda x: x[1]))
    # print(doc_term_freq.cache())

    # Calculate IDFS
    idfs = doc_term_freq.map(lambda x: (x[0], math.log((num_docs / x[1]), 10)))
    l_idfs = idfs.collect()
    # print(l_idfs)
    term_ids = idfs.keys().zipWithIndex().collectAsMap()
    inverse_term_ids = idfs.keys().zipWithIndex().map(lambda x : (x[1], x[0])).collectAsMap()

    # keep a read-only variable cached on each machine rather than shipping a copy of it with tasks. 
    broadcasted_terms_ids = sc.broadcast(term_ids).value
    reverse_broadcasted_ter_ids = sc.broadcast(inverse_term_ids).value

    # print(term_ids.get('insurrection'))
    # print(inverse_term_ids.get(120))
    

    def get_tfidf(doc):
        l = []
        total_num_terms = sum(doc.values())
        for key, value in doc.items():
            if key in broadcasted_terms_ids:
                term_id = broadcasted_terms_ids.get(key)
            else:
                continue
            idf = l_idfs[term_id]
            tf = value
            #term tf-idf
            res = idf[1] * tf / total_num_terms
            l.append((term_id, res))
            
        return SparseVector(len(l_idfs), l)
        

    tf_idf = tf.map(get_tfidf)
    # print(tf_idf.take(1))
    class SVD(JavaModelWrapper):
        """Wrapper around the SVD scala case class"""
        @property
        def U(self):
            """ Returns a RowMatrix whose columns are the left singular vectors of the SVD if computeU was set to be True."""
            u = self.call("U")
            if u is not None:
                return RowMatrix(u)

        @property
        def s(self):
            """Returns a DenseVector with singular values in descending order."""
            return self.call("s")

        @property
        def V(self):
            """ Returns a DenseMatrix whose columns are the right singular vectors of the SVD."""
            return self.call("V")
        
    def computeSVD(row_matrix, k, computeU=False, rCond=1e-9):
        """
        Computes the singular value decomposition of the RowMatrix.
        The given row matrix A of dimension (m X n) is decomposed into U * s * V'T where
        * s: DenseVector consisting of square root of the eigenvalues (singular values) in descending order.
        * U: (m X k) (left singular vectors) is a RowMatrix whose columns are the eigenvectors of (A X A')
        * v: (n X k) (right singular vectors) is a Matrix whose columns are the eigenvectors of (A' X A)
        :param k: number of singular values to keep. We might return less than k if there are numerically zero singular values.
        :param computeU: Whether of not to compute U. If set to be True, then U is computed by A * V * sigma^-1
        :param rCond: the reciprocal condition number. All singular values smaller than rCond * sigma(0) are treated as zero, where sigma(0) is the largest singular value.
        :returns: SVD object
        """
        java_model = row_matrix._java_matrix_wrapper.call("computeSVD", int(k), computeU, float(rCond))
        return SVD(java_model)
    

    start = time.time()
    # 100 ?
    NUM_CONCEPTS = 50

    tf_idf.cache()
    mat = RowMatrix(tf_idf)

    svd = computeSVD(mat, NUM_CONCEPTS, True)

    print(str(time.time() - start) + 's')

    u = svd.U # rows.collect()
    s = svd.s
    v = svd.V

    # distributed
    S_distributed =  sc.parallelize(np.diag(s.toArray())).zipWithIndex()
    V_distributed =  sc.parallelize(v.toArray()).zipWithIndex()
    S_distributed = IndexedRowMatrix( \
        S_distributed \
        .map(lambda row: IndexedRow(row[1], row[0])) \
        ).toBlockMatrix()


    V_distributed = IndexedRowMatrix( \
        V_distributed \
        .map(lambda row: IndexedRow(row[1], row[0])) \
        ).toBlockMatrix()


    # SV_distributed = V_distributed.multiply(S_distributed)

    # local
    V = v.toArray()
    # S = np.diag(s.toArray())

    # SV = np.dot(V, S)
    # SV_normalized = normalize(SV, 'l2')

    # aux = u.rows.map(lambda row: row.toArray())
    # U = np.array(np.array(aux.collect()))

    # US = np.dot(U, S)
    # US_normalized = normalize(US, 'l2')

    # local
    def top_terms_in_concepts(num_results):
        top_terms = []
        
        for compNum in range(num_results):
            comp = V.T[compNum]
            # Sort the weights in the first component, and get the indeces
            indeces = np.argsort(comp).tolist()
            # Reverse the indeces, so we have the largest weights first.
            indeces.reverse()
            terms = []
            weights = []
            for i in indeces[:num_results]:
                term = reverse_broadcasted_ter_ids.get(i) 
                terms.append(term)
            weights = comp[indeces[:10]]
            res = list(zip(terms, weights))
            top_terms.append(res)
            
        return top_terms

    # distributed
    def top_docs_in_concepts(num_results):
        top_docs = []
        for num in range(num_results):
            doc_weights = u.rows.map(lambda i : i.toArray())
            doc_weights_for_concept = doc_weights.map(lambda i : i[num]).zip(doc_titles)
            top_docs_concept = doc_weights_for_concept.top(num_results, lambda i : i[0])
            top_docs.append(top_docs_concept)
            
        return top_docs
    
    # num results
    N = 10
    # if f == 'top_rems_in_concepts':
    spark.stop()
    return top_terms_in_concepts(args["n"])
    # top_concept_docs = top_docs_in_concepts(N)





if __name__ == "__main__":
    # clear_xml('','','')
    # nlp()
    # spark = get_spark()
    pipeline()