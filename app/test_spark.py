from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession
from spark_jobs import summary_stats
from pyspark.mllib.stat import Statistics
from pyspark.sql.types import Row
from pyspark.ml.linalg import Vectors
from pyspark.ml.stat import Summarizer
import numpy as np

conf = SparkConf().setAll(
  [
    ("spark.pyspark.virtualenv.enabled", "true"),
    ("spark.pyspark.virtualenv.bin.path", "/usr/bin/virtualenv"),
    ("spark.pyspark.python", "python3"),
    ("spark.executor.memory", "512m")
  ]
)

spark = (SparkSession.\
        builder.\
        appName("test-spark").\
        master("spark://spark:7077").\
        # config(conf=conf).\
        getOrCreate())
sc = spark.sparkContext


# conf = SparkConf().\
#         setAppName("test-spark").\
#         setMaster("spark://spark:7077")

# sc = SparkContext(conf=conf)

def test_df():
    data = spark.read.csv("/usr/data/iris.data",header=False)\
                    .toDF(
                        'sepal height',
                        'sepal width',
                        'petal length',
                        'petal width',
                        'class'
                    )
    print(data['sepal height'])


def working_example():
	# $example on$
	df = sc.parallelize([Row(weight=1.0, features=Vectors.dense(1.0, 1.0, 1.0)),
							Row(weight=0.0, features=Vectors.dense(1.0, 2.0, 3.0))]).toDF()

	# create summarizer for multiple metrics "mean" and "count"
	summarizer = Summarizer.metrics("mean", "count")

	# compute statistics for multiple metrics with weight
	df.select(summarizer.summary(df.features, df.weight)).show(truncate=False)

	# compute statistics for multiple metrics without weight
	df.select(summarizer.summary(df.features)).show(truncate=False)

	# compute statistics for single metric "mean" with weight
	df.select(Summarizer.mean(df.features, df.weight)).show(truncate=False)

	# compute statistics for single metric "mean" without weight
	df.select(Summarizer.mean(df.features)).show(truncate=False)
	# $example off$

	spark.stop()


if __name__ == "__main__":
    data = spark.read.csv("/usr/data/iris.data",header=False)
                    
    # data.show()
    rdd = spark.sparkContext.parallelize(data).toDF(
                        'sepal height',
                        'sepal width',
                        'petal length',
                        'petal width',
                        'class'
                    )
    summary = Statistics.colStats(rdd)
    print(summary.mean())
    print(summary.variance())
    print(summary.numNonzeros())